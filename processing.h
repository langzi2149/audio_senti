/***********************************************************
 * Ed Ang
 * Date: 22 Apr 2021
 * Pseudocode used to process raw incoming data. Consists of
 * 2 stages - a SAD adaptive filter followed by a compression
 * algorithm.
 * Note that all preprocessor vars are capitalized.
 ***********************************************************/

#ifndef PROCESSING_H
#define PROCESSING_H

// Filter functions.
void processing_init_filter();
void processing_process(int16_t incoming_signal, int16_t noise_ref, int16_t *result);

// Not implemented. Should be an FIR filter with sufficient roll-off.
// Primarily used as a anti-alising filter.
void processing_lowPassFilter(int16_t* incoming, uint16_t cutOffHz);

// Not implemented. Simply select one in every 10 ssamples.
void processing_downSample(int16_t incoming, int16_t outgoing);

// Compression functions.

// Packet structure.
typedef struct {
  uint32_t token[4];
  uint8_t packet_counter;
  uint8_t packet_type;
  uint16_t packet_size;
  uint8_t node_position; //Indicates which node the data originates from.
  uint32_t crc;
  uint8_t encoded[MAX_BLOCK_SIZE];
}packet_t;

void processing_mpeg_encoder(int16_t *input);
void processing_build_packet();
uint8_t* processing_get_packet();

#endif
