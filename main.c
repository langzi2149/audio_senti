/***********************************************************
 * Ed Ang
 * Date: 22 Apr 2021
 * Pseudocode used to process raw incoming data. Consists of
 * 2 stages - a SAD adaptive filter followed by a compression
 * algorithm.
 * Note that all preprocessor vars are capitalized.
 ***********************************************************/

//Include all necessary headers including freertos.
#include ...
//There should be a lower-level driver file 'modem_drivers.c' for UART communications with the modem.
//A higher-level device agnostic API file 'modem_api.c' with associated header should be present and
//included at application level.
#include "modem_api.h"
#include "processing.h"

static uint8_t poll_buffer_flag = 0;     //Flag used to implement a switched buffer scheme.
static uint8_t process_buffer_flag = 0;  //Flag used to implement a switched buffer scheme.
static QueueHandle_t q1, q2;             //These are the buffers used in the switched buffer scheme.
static uint16_t node_data1[MINI_BLOCK_SiZE];
static uint16_t node_data2[MINI_BLOCK_SiZE];
static uint8_t modem_up = 0x00;

// This function downsamples, filters, and compresses the data.
packet_t* process_incoming(int16_t *incoming_raw, int16_t noise_ref_raw) {
  uint16_t i;
  int16_t incoming[DOWNSAMPLED_SIZE];   //Store downsampled raw data.
  int16_t noise_ref[DOWNSAMPLED_SIZE];  //Store downsampled noise data.
  int16_t filtered[DOWNSAMPLED_SIZE];
  
  uint16_t cutOffHz = 2000; //Remove high frequency components to prevent aliasing.
  processing_lowPassFilter(incoming_raw, cutOffHz);  //In-place low pass filtering.
  processing_downSample(incoming_raw, &incoming);     //Take 1 in every 10 samples.
  
  processing_lowPassFilter(noise_ref_raw, cutOffHz);
  processing_downSample(noise_ref_raw, &noise_ref);
  
  processing_process(&incoming, &noise_ref, &filtered);
  
  // Compress filtered data into a packet.
  processing_mpeg_encoder(&filtered);
  
  processing_build_packet();
  
  return(processing_get_packet());
}

// Functions used to handle modem operations.
void init_modem() {
  uint8_t error_code = 0;
  
  // Used to define end point and cipher scheme etc.
  error_code |= modem_configure_ssl();
  error_code |= other_init_tasks();
  error_code |= modem_connect();
  
  // Retrieve hashed token from flash unique to this particular control module.
  // Token written to non-volatile memory at production, and stored to backend
  // database. Independent of keys used to establish TLS connection.
  token = retrieve_production_token();
  error_code |= modem_authenticate(token);
  
  // Create timer interrupt that closes connection after a certain time elapses.
  init_timer_interrupt(CONNECTION_OPEN_IDLE_MAX, close_connection_callback);
  
  return(error_code);
}

// This function sends the data over the LTE modem via UART.
void send_packet() {
  packet_t* packet = processing_get_packet();

  error_code = check_modem_up();

  if(!error_code) {
    // Connection down. Reconnect ...
    error_code = modem_connect();
  } else {
    // This resets a timer used to close the connection
    // if there is no activity on the connection.
    // i.e. connection will be kept open.
    modem_reset_timer(CONNECTION_OPEN_IDLE_MAX);
  }
  
  // Send payload over secure connection.
  if(error_code) { 
    error_code |= modem_send(packet, packet->size);
  } else {
    return(error_code);
  }
  
  return(error_code);
}

// Time slicing mode is used here. 
// configUSE_TIME_SLICING is set to 1.
// A simple if-else scheme is used here to maintain control
// for illustration purposes.
// In reality, a switch based state machine will probably be
// needed to take into account more varied and complex
// situations.
void polling_nodes_task( void *pvParameters ) {
  int16_t* ndata;
  
  q1 = xQueueCreate(Q_SIZE, sizeof(uint16_t *));
  q2 = xQueueCreate(Q_SIZE, sizeof(uint16_t *));

  xLastWakeTime = xTaskGetTickCount();
  
  for(;;) {
    vTaskDelayUntil( &xLastWakeTime, ACCEPTABLE_DELAY );

    // get_node is used to poll each node and retrieve 30s of data
    // in small blocks of ~100 ms each.
    // Each function call will result in 100 ms of data that will
    // be stored in the array.
    // Automatic switching to a different node will be done within
    // function.
    
    if(poll_buffer_flag == 0) {
      get_node(&node_data1);
      xQueueSend(q1, &node_data1, (TickType_t) 0);
      ndata = &node_data1;
      poll_buffer_flag = 1;
    } else {
      get_node(&node_data2);
      xQueueSend(q2, &node_data2, (TickType_t) 0);
      ndata = &node_data2;
      poll_buffer_flag = 0;
    }

    // Store node data to external memory.
    store_ext_mem(ndata);
    
  }

}

void processing_task( void *pvParameters ) {
  
  for(;;) {

    // Check Q1 for data. Do nothing if Q empty.
    // The separate function divides the data into raw
    // incoming signal and its noise reference.
    // MAX_DELAY suitably set to ensure frequency of q
    // check isn't too high but doesn't block excessively.
    if( xQueueReceive( q1, &node_data, MAX_DELAY ) ) {
      separate(node_data, &raw, &noise);
      process_incoming(&raw, &noise);
      send_packet();
    }
    
    // As above for data in Q2.
    if( xQueueReceive( q2, &node_data, MAX_DELAY ) ) {
      separate(node_data, &raw, &noise);
      process_incoming(&raw, &noise);
      send_packet();
    }
  }
}

void main() {
  
  // All initialisation tasks that are done once.
  init_modem();
  init_watchdog();
  init_bleStack();
  init_filter();
  init_all_other_modules();
  
  // Create tasks of equal priorties.
  // Note that additional arguments are not specified here.
  xTaskCreate(polling_nodes_task, ...);
  xTaskCreate(processing_task, ...);
  
  vTaskStartScheduler();
  
  while(true) {}

}
