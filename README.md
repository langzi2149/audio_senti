# Introduction

A conceptualized version of a firmware architecture is shown here. The purpose of this sub-system is to process the incoming audio signal, store it to external memory, and relay the downsampled data to the backend via a secure connection. The following assumptions are made here.

1. Two mono PCM 16-bit audio streams (sampling rate 44100 Hz) that form a stereo pair are being retrieved - they are originally from two microphones, one closer to the chest and therefore able to pick up respiratory sounds and one further away that acts as a noise reference signal.
2. The two audio streams are correlated. Both microphones pick up both the intended signal as well as noise, although their relative amplitudes are different.
3. Streaming of audio data has to be done 'concurrently' with the acquisition (polling) of audio stream from each node (or module that picks up audio from different body locations).
4. It is possible to make use of a switched buffer scheme, where polled audio data is stored in small buffer (SRAM). When the buffer is full, seamless switching to another buffer is then possible.
5. In terms of resourcing, it is possible to deploy an RTOS onto the MCU. It is also feasible to put the RTOS in time slicing mode. Concurrency in terms of memory access will be handled properly.
6. It is necessary to compress the data before sending it to the backend. The compression algorithm should be able to work with small blocks of data to enable the live streaming requirement.
7. The LTE modem is connected via UART, and AT commands are sent via the UART serial protocol to the modem.

# Firmware Architecture

The architecture shown below is incomplete as it describes a subsystem. Deploying the system as an RTOS is useful as issues with concurrent memory accesses and division of CPU time between tasks can be handled by the underlying kernel. The firmware is divided broadly into drivers and the application layer, where the drivers include the BLE stack, UART based drivers for the LTE modem, and drivers for the high capacity data storage (this could take the form of an onboard SD card for audio data storage). The low-level drivers are accessed by the application through an API layer, which should be agnostic to the underlying hardware device. The application layer contains the necessary audio processing algorithms as well as the control logic.   

![firmware_arch](Diagrams/senti_firmware_architecture.png)

The following strategies are employed in this architecture.
1. A switched buffer is employed here to capture data that has been been polled via BLE. Specifically, the captured data is temporarily buffered and pushed onto a small queue. The data acquisition continues after the buffer is full but it pushes the data onto a new queue. The reason it does that is because concurrent processing of the data on the previous queue can then take place since the system is deployed in time slicing mode. Switching is done back and forth so that the polling task and processing task never operates on the same buffer.
2. The processing task will check both queues to see if there is any available blocks of data to process. Otherwise, it does nothing. Block size can be selected to ensure that the time taken for the polling and processing tasks are similar, and that latency on the receiving end is acceptable.
3. A TCP based SSL connection will be established with the backend in this case. The relevant information such as the end point, cipher type, and other authentication information will be stored in the modem_api.h header file. This configuration information will be provided in the function call 'modem_configure_ssl()'.
4. After opening a socket, connecting/authenticating the backend, client authentication is done by using a token that is stored in non-volatile memory during production. This token is unique to the control module and stored in the backend. This will allow association with other patient information on the backend.
5. Data transfer is carried out using the function call 'send_packet()'. A connection will persist so long as data streaming is active. An idle connection cannot persist as the connection will close if the timer is not reset (done on every call to the send_packet() function).
6. Incoming signals are low pass filtered to about 2 kHz before down-sampling to remove aliasing noise. Downsampling the signals at the start will allow downstream computations to be substantially reduced as well.

# Noise removal

It is assumed that two channels of audio data are available, one for the original audio signal and the other as a noise reference. Since both microphones are closely located, it is likely that both will pick up both the noise as well as the intended signal (respiratory sounds). In consequence, the conventional adaptive filter is unlikely to work well enough, as it assumes that the noise reference is uncorrelated with the original signal. Therefore, I make use of the Symmetric Adaptive Decorrelation filter here (figure below). It is a modified version of an adapative filter that carries out blind source separation and the outputs will consist of the denoised audio and the noise signal (Gerven et. al., "Signal Separation by Symmetric Adaptive Decorrelation: Stability, Convergence, and Uniqueness", IEEE Transactions on Signal Processing, 1995). This approach makes sparing use of computational resources but it requires more evaluation. Other methods for real-time blind source separation do exist, such as the extended Kalman filter.

![sad_filter](Diagrams/SAD_filter.png)

# Audio compression

The MPEG audio compression is a famously effective algorithm. Unlike the part of the algorithm that does video compression, MPEG audio compression is a lossy method and the reason why it achieves good reconstruction performance and high compression ratios is because of the pychoacoustic filters that are used in conjunction with quantization to attenuate audio that cannot be perceived. The challenge is to be able to implement the encoder in real-time, operating on small blocks of data and achieve good compression ratios. Rather than building one from scratch, adapting other off-the-shelf [compression algorithms](https://opus-codec.org/) might be the sensible approach.

![sad_filter](Diagrams/encoder.png)
