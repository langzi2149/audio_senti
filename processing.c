/***********************************************************
 * Ed Ang
 * Date: 22 Apr 2021
 * Pseudocode used to process raw incoming data. Consists of
 * 2 stages - a SAD adaptive filter followed by a compression
 * algorithm.
 * Note that all preprocessor vars are capitalized.
 ***********************************************************/

#include "processing.h"

// Conversion to fixed-point arithmetic could be an option.
// Circular buffers to avoid shift implementations.
static float w_signal_coeffs[W_NUM_STATES_SIGNAL];     // FIR filter coeffs for the signal.
static float w_signal_state[W_NUM_STATES_SIGNAL];     // FIR filter state for the signal.
static uint8_t w_signal_index =0;

static float w_noise_ref_coeffs[W_NUM_STATES_NOISE];   // FIR filter coeffs for the noise reference.
static float w_noise_ref_state[W_NUM_STATES_NOISE];   // FIR filter state for the noise reference.
static uint8_t w_noise_index =0;

static float u_signal_state[U_NUM_STATES_SIGNAL];     // State maintaining errors from the signal error.
static uint8_t u_signal_index = 0;

static float u_noise_ref_state[U_NUM_STATES_NOISE];   // State maintaining errors from the NOISE reference.
static uint8_t u_noise_index = 0;

static float outputs[W_NUM_STATES_SIGNAL + W_NUM_STATES_NOISE];
static uint8_t outputs_index = 0;

static float u1 = 0.0;
static float u2 = 0.0;

static void weight_update();

/***********************************************************
 * Symmetric Adaptive Decorrelator Filter.
 ***********************************************************/

void processing_init_filter() {
  uint8_t i;
  
  w_signal_state[0] = 0.0;
  for(i=1; i<W_NUM_STATES_SIGNAL; ++i) {
    w_signal_state[i] = 1.0/(W_NUM_STATES_SIGNAL - 1);
    u_signal_state[i] = 0.0;
  }
  
  for(i=0; i<W_NUM_NOISE_REF_SIGNAL; ++i) {
    w_noise_ref_state[i] = 1.0/W_NUM_STATES_NOISE;
    u_noise_ref_state[i] = 0.0;
  }
}

void processing_process(int16_t incoming_signal, int16_t noise_ref, int16_t *result) {
  
  // Compute errors for the signal and noise paths.
  w_signal_state[w_signal_index] = (float)incoming_signal;
  update_buffer_index(&w_signal_index);
  for(i=0; i<W_NUM_STATES_SIGNAL; ++i) {
    u2 += w_signal_state[i]*w_signal_coeffs[i]; 
  }
  u2 = (float)noise_ref - u2;
  u_noise_ref_state[u_noise_index] = u2;
  update_buffer_index(&u_noise_index);
  
  w_noise_ref_state[w_signal_index] = (float)noise_ref;
  update_buffer_index(&w_noise_ref_index);
  for(i=0; i<W_NUM_STATES_NOISE; ++i) {
    u1 += w_noise_ref_state[i]*w_noise_ref_coeffs[i]; 
  }
  u1 = (float)incoming_signal - u1;
  u_signal_state[u_signal_state_index] = u1;
  update_buffer_index(u_signal_state_index);
    
  // Obtain the corrected signal.
  float result_float = 0.0;
  for(i=0; i<W_NUM_STATES_SIGNAL; ++i) {
    result_float += align_signal_outputs(w_signal_coeffs[i], outputs[i]); 
  }

  uint8_t output_index = W_NUM_STATES_SIGNAL;
  for(i=0; i<W_NUM_STATES_NOISE; ++i) {
    result_float += align_noise_outputs(w_noise_ref_coeffs[i]*outputs[output_index]);
    ++output_index;
  }

  result_float = (float)incoming_signal - result_float;

  // Rounding before conversion to integer.
  if(result_float < 0.0) {
    result_float -= 0.5;
  } else {
    result_float += 0.5;
  }

  outputs[outputs_index] = result_float;
  update_buffer_index(&outputs_index);

  // Carry out the weight update.
  weight_update();
  
  return((int16_t)result_float);
}

static void weight_update() {

  for(i=0; i<W_NUM_STATES_SIGNAL; ++i) {
    w_signal_state[i] += TRAINING_STEP_SIGNAL*u1*u_noise_ref_state[i]; 
  }
  
  for(i=0; i<W_NUM_STATES_NOISE; ++i) {
    w_noise_ref_state[i] += TRAINING_STEP_NOISE*u2*u_signal_state[i]; 
  }
  
}

/***********************************************************
 * MPEG audio compression.
 ***********************************************************/

static packet_t packet;
static uint8_t packet_counter = 0;
static uint16_t packet_length;

// Produces a 4-byte sequence to enable CRC check on the receiving end. 
static void crc32(uint8_t *input, uint8_t len, uint8_t *ouput);

// Conceptual version of a 'real-time' MPEG encoder shown here.
// Raw block size could be 512 ~ 125 ms of data.
// Rather than building a compression module from scratch. Off-the-shelf open
// source compression algorithms could be evaluated for use here.
// One option that has been adapted to the M4 here:
// https://opus-codec.org/
void  processing_mpeg_encoder(int16_t *input) {
  uint8_t i;
  int16_t filtered[32];
  int16_t scale_factors[32];

  for(i=0; i<RAW_BLOCK_SIZE; ++i) {

    //32 banks in total for the same input.
    filterbank(input[i], &filtered);

    psychoacoustic_model(input[i], &scale_factors);

    //In-place quantization and coding.
    quantize(&filtered, &scale_factors);

    // First 4 bytes used for CRC check bytes.
    packet_length = coding(&filtered, &packet.encoded);
  }

  //Create CRC check bytes.
  crc32(packet_ptr.encoded, packet_length, &packet_ptr.crc);

}

void  processing_build_packet() {
  // Form packet header.
  
  // Retrieve token from specific location in flash memory (written during production).
  // Token should be a 128-bit UUID, used for authentication on the backend.
  // Written direct to first 16 bytes in the packet.
  // The assumption would be to send it with every packet but this can be changed.
  retrieve_token(&packet.token);

  // Quick scheme to identify packet loss on the receiving end.
  packet.packet_counter = packet_counter;

  // Overflow will bring it back to 0.
  packet_counter++; 
  
  // Packet type.
  packet.packet_type = DATA_PACKET_TYPE;

  // Node identity (Origin of data).
  packet.node_position = get_node_position();

  // Packet size.
  packet_length += (HEADLER_LEN + 4);
  packet.packet_size = packet_length;
}

packet_t*  processing_get_packet() {
  return &packet;
}
